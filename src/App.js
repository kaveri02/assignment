import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = { fromdate: null, todate: null, result: null };
    this.FromChangeValue = this.FromChangeValue.bind(this);
    this.toChangeValue = this.toChangeValue.bind(this);
    this.Clickbutton = this.Clickbutton.bind(this);
  }
  FromChangeValue(event) {
    this.setState({ fromdate: event.target.value })
  }
  toChangeValue(event) {
    this.setState({ todate: event.target.value })
  }
  Clickbutton(event) {
    var fromDT = this.state.fromdate;
    var toDT = this.state.todate;
    var fromDAY = "";
    var fromMONTH = "";
    var fromYEAR = "";
    var toDAY = "";
    var toMONTH = "";
    var toYEAR = "";
    var errorFlag = "False";
    var isNullFlag = "False";

    if (fromDT === null || toDT === null || fromDT === '' || toDT === '')
     {
      isNullFlag = "True";
    } 
    else
    {
      fromDAY = fromDT.substring(0, 2);
      fromMONTH = fromDT.substring(3, 5);
      fromYEAR = fromDT.substring(6, 10);

      toDAY = toDT.substring(0, 2);
      toMONTH = toDT.substring(3, 5);
      toYEAR = toDT.substring(6, 10);
    }

    if (isNullFlag === "True")
     {
      alert("Alert! From Date / To Date is empty .Please enter both dates");
      errorFlag = "True";
    }
    else if (
      fromDT.length !== 10 || toDT.length !== 10 || fromDT.substring(2, 3) !== ' '
      || fromDT.substring(5, 6) !== ' ' || toDT.substring(2, 3) !== ' ' || toDT.substring(5, 6) !== ' '
      || isNaN(fromDAY) || isNaN(fromMONTH) || isNaN(fromYEAR)
      || isNaN(toDAY) || isNaN(toMONTH) || isNaN(toYEAR)
    ) {
      alert("Alert! Invalid date format! Please enter date in correct format DD MM YYYY");
      errorFlag = "True";
    }
    else if (fromMONTH < 1 || fromMONTH > 12 || toMONTH < 1 || toMONTH > 12) {
      alert("Alert! Please enter valid month 01-12 ");
      errorFlag = "True";
    }
    else if (fromYEAR < 1900 || fromYEAR > 2010 || toYEAR < 1900 || toYEAR > 2010) {
      alert("Alert! Please enter dates between 01 01 1900-31 12 2010")
      errorFlag = "True";
    }
    else {

      var isFromLeapYear = (fromYEAR % 100 === 0) ? (fromYEAR % 400 === 0) : (fromYEAR % 4 === 0);
      var isToLeapYear = (toYEAR % 100 === 0) ? (toYEAR % 400 === 0) : (toYEAR % 4 === 0);

      switch (fromMONTH) {
        case '01':
        case '03':
        case '05':
        case '07':
        case '08':
        case '10':
        case '12':
          if (fromDAY < 1 || fromDAY > 31) {
            alert("Based on From Month, Day range allowed 1-31");
            errorFlag = "True";
          }
          break;
        case '04':
        case '06':
        case '09':
        case '11':
          if (fromDAY < 1 || fromDAY > 30) {
            alert("Based on From Month, Day range allowed 1-30");
            errorFlag = "True";
          }

          break;
        case '02':
          if (isFromLeapYear) {
            if (fromDAY < 1 || fromDAY > 29) {
              alert("Alert! FromDate Feb (LeapYear), Day range allowed 1-29");
              errorFlag = "True";
            }
          }
          else if (fromDAY < 1 || fromDAY > 28) {
            alert("Alert! FromDate Feb (Non-LeapYear), Day range allowed 1-28");
            errorFlag = "True";
          }
          break;
          default:
          break;
      }


      switch (toMONTH) {
        case '01':
        case '03':
        case '05':
        case '07':
        case '08':
        case '10':
        case '12':
          if (toDAY < 1 || toDAY > 31) {
            alert("Based on From Month, Day range allowed 1-31");
            errorFlag = "True";
          }
          break;
        case '04':
        case '06':
        case '09':
        case '11':
          if (toDAY < 1 || toDAY > 30) {
            alert("Based on From Month, Day range allowed 1-30");
            errorFlag = "True";
          }

          break;
        case '02':
          if (isToLeapYear) {
            if (toDAY < 1 || toDAY > 29) {
              alert("Alert! ToDate Feb (LeapYear), Day range allowed 1-29");
              errorFlag = "True";
            }

          }
          else if (toDAY < 1 || toDAY > 28) {
            alert("Alert! ToDate Feb (Non-LeapYear), Day range allowed 1-28");
            errorFlag = "True";
          }
          break;
          default:
          break;
      }


    }

    if (errorFlag === "False") {
      var calFromDate = fromMONTH + '/' + fromDAY + '/' + fromYEAR;
      var calToDate = toMONTH + '/' + toDAY + '/' + toYEAR;

      var newFromDate = new Date(calFromDate);
      var newToDate = new Date(calToDate);

      var timeDiff = Math.abs(newToDate.getTime() - newFromDate.getTime());
      var res = Math.ceil(timeDiff / (1000 * 3600 * 24));
      var finalmsg = null
      if (res != null) {
        finalmsg = fromDT + ' , ' + toDT + ' , ' + res;
        this.setState({ result: finalmsg });
      }


    }



  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>
            Date Difference Calculator
        </h1>
          <table>
            <tr>
              <td>
                <label>
                  From Date
            </label>
              </td>
              <td>
                <input type="text" placeholder="DD MM YYYY" title="Enter a date in this format DD MM YYYY" value={this.state.fromdate}
                  onChange={this.FromChangeValue}>
                </input>
              </td>
            </tr>
            <tr>
              <td>
                <label>
                  To Date
            </label>
              </td>
              <td>
                <input type="text" placeholder="DD MM YYYY" title="Enter a date in this format DD MM YYYY" value={this.state.todate}
                  onChange={this.toChangeValue}>
                </input>
              </td>
            </tr>
            <tr>
              <td>
              </td>
              <td>
                <button onClick={this.Clickbutton}>
                  Calculate
              </button>
              </td>
            </tr>
            <tr>
              <td>
                <label>
                  Result
            </label>
              </td>
              <td>
                {this.state.result}
              </td>
            </tr>
          </table>
        </header>
      </div>
    );
  }
}

export default App;
